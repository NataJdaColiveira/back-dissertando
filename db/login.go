package db

import (
	"dissertando/model"
	"errors"
	"log"

	"golang.org/x/crypto/bcrypt"
)

func Register(userData model.User) error {
	db := InitDb()
	users:= []model.User{}

	err := db.Select(&users, "SELECT * FROM person WHERE email = $1", userData.Email)
	if err != nil {
		log.Println(err)
		return  err
	}

	if(len(users) >= 1) {
		err = errors.New("este e-mail já está sendo utilizado")
		return  err
	}

	tx := db.MustBegin()

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(userData.Password), 8)
	if err != nil {
		return err
	}

	tx.MustExec("INSERT INTO person (name, email, password) VALUES ($1, $2, $3)", userData.Name, userData.Email, string(hashedPassword))

	tx.Commit()

	return nil
}

func Login(loginInput model.User) (model.User, error) {
	db := InitDb()
	users:= []model.User{}
	user := model.User{}
	password := []byte(loginInput.Password)

	err := db.Select(&users, `
		SELECT 
			name as name,
			email as email,
			role as role,
			password as password
		FROM
			person 
		WHERE 
			email = $1`,
	loginInput.Email)
	if err != nil {
		log.Println(err)
		return model.User{}, err
	}

	if(len(users) == 0) {
		err = errors.New("nenhum usuário com este e-mail foi encontrado")
		return model.User{}, err
	}

	if(len(users) > 1 ) {
		err = errors.New("credenciais inválidas")
		return model.User{}, err
	}

	user = users[0]

	err = bcrypt.CompareHashAndPassword( []byte(user.Password), password)
	if err != nil {
		err = errors.New("credenciais inválidas")
		return model.User{}, err
	}
	
	user.Password = ""

	return user, nil
}
