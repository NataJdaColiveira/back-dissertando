package db

import (
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)


func InitDb() *sqlx.DB {
	db, err := sqlx.Connect("postgres", "host=localhost port=5432 user=postgres password=159753Hacked dbname=dissertando sslmode=disable")
	if err != nil {
		log.Fatalln(err)
	}

	return db
}