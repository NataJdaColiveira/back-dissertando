package model

type User struct {
	UUID     string `db:"uuid" json:"-"`
	Name     string `db:"name" json:"name"`
	Email    string `db:"email" json:"email"`
	Password string `db:"password" json:"-"`
	Role     int    `db:"role" json:"role"`
}

// seperar alunos e professores para o admin
// lista de redações por aluno