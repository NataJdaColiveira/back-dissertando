package handler

import (
	"dissertando/db"
	"dissertando/model"
	"dissertando/tools"
	"encoding/json"
	"net/http"

	"github.com/labstack/echo/v4"
)

func PostRegisterHandler(c echo.Context) error {
	jsonMap := make(map[string]interface{})

	if err := json.NewDecoder(c.Request().Body).Decode(&jsonMap); err != nil {
		return err
	} else {
		name := jsonMap["name"].(string)
		email := jsonMap["email"].(string)
		password := jsonMap["password"].(string)

		if name == "" || email == "" || password == "" || len(password) < 6{
			return c.String(http.StatusBadRequest, "Dados inválidos")
		}
		
		user := model.User {
			Name: name,
			Email: email,
			Password: password,
		}
		
		err = db.Register(user)
		if err != nil {
			c.JSON(http.StatusBadRequest, err.Error())
			return err
		}
		
		return c.JSON(http.StatusOK, "cadastro realizado com sucesso")
	}
}

func PostLoginHandler(c echo.Context) error {
	jsonMap := make(map[string]interface{})

	if err := json.NewDecoder(c.Request().Body).Decode(&jsonMap); err != nil {
		return err
	} else {
		email := jsonMap["email"].(string)
		password := jsonMap["password"].(string)

		if  email == "" || password == "" || len(password) < 6{
			return c.String(http.StatusBadRequest, "Dados inválidos")
		}
		
		user := model.User {
			Email: email,
			Password: password,
		}
		
		user, err := db.Login(user)
		if err != nil {
			c.JSON(http.StatusUnauthorized, err.Error())
			return err
		}

		token, err := tools.CreateToken(user.UUID)
		if err != nil {
			c.JSON(http.StatusUnauthorized, err.Error())
			return err
		}

		return c.JSON(http.StatusOK, map[string]interface{}{
			"token": token,
			"user": user,
		})	
	}
}

// func getEstadosHandler(c echo.Context) error {
// 	estados, err := db.GetAllEstados()
// 	if err != nil {
// 		return c.String(http.StatusBadRequest, err.Error())
// 	}
// 	return c.JSON(http.StatusOK, estados)
// }