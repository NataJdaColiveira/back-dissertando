package main

import (
	"dissertando/handler"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	os.Setenv("ACCESS_SECRET", "@NcRfU5u7xq3t6w9!A%Zz$jXn2rC&F)J")
	e := echo.New()

   e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
      AllowOrigins: []string{"*"},
      AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
    }))

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	jwtGroup := e.Group("/jwt")
	jwtGroup.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningMethod: "HS256",
		SigningKey:    []byte(os.Getenv("ACCESS_SECRET")),
	}))
	
	e.POST("/registro", handler.PostRegisterHandler)
	e.POST("/login", handler.PostLoginHandler)

	// e.GET("/municipios", getMunicipiosHandler)
	// e.GET("/estados", getEstadosHandler)

	// jwtGroup.POST("/municipios", postMunicipiosHandler)
	// jwtGroup.PUT("/municipios", updateMunicipiosHandler)
	// jwtGroup.DELETE("/municipios", deleteMunicipiosHandler)

	// jwtGroup.POST("/estados", postEstadosHandler)
	// jwtGroup.PUT("/estados", updateEstadosHandler)
	// jwtGroup.DELETE("/estados", deleteEstadosHandler)

	e.Logger.Fatal(e.Start(":1323"))
}